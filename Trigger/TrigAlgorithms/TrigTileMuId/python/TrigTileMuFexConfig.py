# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

from TrigTileMuId import TrigTileMuIdMonitoring
from TrigTileMuId import TrigTileMuIdConf

class TrigTileMuFexConfig (TrigTileMuIdConf.TrigTileMuFex):
    __slot__ = []

    def __new__( cls, *args, **kwargs ):

       if len(args) == 2:
         newargs = ['%s_%s_%s' % (cls.getType(),args[0],args[1])] + list(args)
       else:
         newargs = ['%s_%s' % (cls.getType(),args[0]) ] + list(args)

       return super( TrigTileMuFexConfig, cls ).__new__( cls, *newargs, **kwargs )

    def __init__(self, name, *args, **kwargs ):
        super( TrigTileMuFexConfig, self ).__init__( name )

        TrigTileMuIdConf.TrigTileMuFex.__init__(self, name)

        self.UseAthenaFieldService = True

        self.IDalgo = "IDSCAN" 
        if len(args) == 2:
          TrackAlgo = args[1]
          if (TrackAlgo=='SITRACK'):
            self.IDalgo = "SITRACK"
          if (TrackAlgo=='TRTXK'):
            self.IDalgo = "TRTXK"
          if (TrackAlgo=='TRTSEG'):
            self.IDalgo = "TRTSEG"

        #self.IDalgo="SITRACK"		# After "TrigSiTrack_Muon"
        self.Meth_ExtrapTileR = "DirectPars"
        #self.Meth_ExtrapTileR = "EMextraWay"
        self.DelPhi_Cut = 0.2
        self.DelEta_Cut = 0.1
        self.Pt_Cut = 2000.0
        # Unit(Pt) : MeV

        self.GetTruthMuon = False
        #self.GetTruthMuon = True 

        validation = TrigTileMuIdMonitoring.TrigTileMuFexValidationMonitoring()
        online     = TrigTileMuIdMonitoring.TrigTileMuFexOnlineMonitoring()
        cosmic     = TrigTileMuIdMonitoring.TrigTileMuFexCosmicMonitoring()

        from TrigTimeMonitor.TrigTimeHistToolConfig import TrigTimeHistToolConfig
        time = TrigTimeHistToolConfig("Time")
        time.TimerHistLimits = [0, 70]
 
        self.AthenaMonTools = [ validation, online, cosmic, time ]  
